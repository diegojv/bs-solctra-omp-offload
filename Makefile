COMPILER ?= amd
TRACE ?= false

ifeq ($(COMPILER),nvidia)
CC = nvc++	
CCFLAGS = -O3 -mp=gpu -gpu=pinned,fastmath -Minfo=mp
LDFLAGS = -lnvToolsExt -L/work/djimenez/nvhpc_2022_221_Linux_x86_64_cuda_11.5/installdir/Linux_x86_64/22.1/cuda/11.5/lib64
endif

ifeq ($(COMPILER),intel)
CC = icpx	
CCFLAGS = -O3 -qopenmp -fopenmp-targets=spir64
endif

ifeq ($(COMPILER),amd)
CC =amdclang++
ifeq ($(TRACE),true)
ROCM_PATH = /global/software/rocm/rocm-5.2.3
INC_PATH  ?= $(ROCM_PATH)/include/roctracer/
LIB_PATH ?= $(ROCM_PATH)/lib
CCFLAGS = -DTRACE_ROCTX -g -O3 -Ofast -ffast-math -target x86_64-pc-linux-gnu -fopenmp -fopenmp-targets=amdgcn-amd-amdhsa -Xopenmp-target=amdgcn-amd-amdhsa -march=gfx90a  
INCLUDES = -I$(INC_PATH)
LDFLAGS =  -Wl,--rpath,${LIB_PATH} $(LIB_PATH)/libroctracer64.so $(LIB_PATH)/libroctx64.so -lamdhip64 -L$(ROCM_PATH)/lib
endif
ifeq ($(TRACE),false)
CCFLAGS = -O3 -Ofast -ffast-math -target x86_64-pc-linux-gnu -fopenmp -fopenmp-targets=amdgcn-amd-amdhsa -Xopenmp-target=amdgcn-amd-amdhsa -march=gfx90a 
endif
endif

#C1=mpicxx

EXE = bs-gpu

SOURCES = utils.cpp solctra_multinode.cpp main_multinode.cpp
OBJECTS = $(SOURCES:.cpp=.o)

$(EXE): $(OBJECTS)
	$(CC) $(CCFLAGS) $(INCFLAGS) -o $@ $^ $(LDFLAGS)

%.o: %.cpp
	$(CC) $(CCFLAGS) $(INCLUDES) -c $< -o $*.o

#bin:
#	$(C1) -O3 -std=c++0x bin_reader.cpp -o read_bins

clean:
	-rm $(OBJECTS)
	-rm $(EXE)
