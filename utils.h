//
// utils.h
// Created by Diego Jimenez
// Basic simulation structures and function prototypes declarations
//

#ifndef SOLCTRA_UTILS_H
#define SOLCTRA_UTILS_H
#include <sstream>
#include <cstdio>
#include <string>
#ifdef TRACE_ROCTX
#include "roctx.h"
#include "roctracer_ext.h"
#endif
#define PI      3.141592654
#define miu     1.2566e-06
#define I       -4350
#define MINOR_RADIUS 0.0944165
#define MAJOR_RADIUS 0.2381
#define ALIGNMENT_SIZE 64
#ifdef KNL
#define GRADES_PER_PAGE ALIGNMENT_SIZE  * KNL / sizeof(double)
#else
#define GRADES_PER_PAGE ALIGNMENT_SIZE / sizeof(double)
#endif
#define DIMENSIONS 3
#define TOTAL_OF_GRADES 360
#define LOOP_OF_GRADES 361
#define TOTAL_OF_GRADES_PADDED 384
#define MULTIPLIER (miu*I)/(4*PI)
#define TOTAL_OF_COILS 12
//old const double PI  = 3.141592654;
//old const double miu = 1.2566e-06;
//old const int I = -4350;
//old const double MINOR_RADIUS = 0.0944165;
//old const double MAJOR_RADIUS = 0.2381;
//old const double ALIGNMENT_SIZE = 64;
//old const int DIMENSIONS =3;
//old const int TOTAL_OF_GRADES= 360;
//old const int LOOP_OF_GRADES= 361;
//old const int TOTAL_OF_GRADES_PADDED= 384;
//old const int MULTIPLIER = (miu*I)/(4*PI);
//old const int TOTAL_OF_COILS = 12;

struct cartesian
{
    double x, y, z;
    void print()
    {
        printf("X=[%.17g]. Y=[%.17g]. Z=[%.17g].\n", x, y, z);
    }
};


struct Particle
{
    double x, y, z;
};


struct Coil
{
    double* x;
    double* y;
    double* z;

};

struct GlobalData
{
    Coil coils;
    Coil e_roof;
    double* leng_segment;
};

#ifndef __INTEL_COMPILER

void* _mmm_malloc(size_t size, size_t alignment);
void _mmm_free(void* pointer);

#define nullptr NULL

#endif


void loadCartesianFile(double* coils, const int length, const std::string& path);
void loadParticleFile(double* particles, const int length, const std::string& path);
double getCurrentTime();
void createDirectoryIfNotExists(const std::string& path);
bool directoryExists(const std::string& path);
std::string getZeroPadded(const int num);
double randomGenerator(const double min, const double max, const int seedValue);
void initializeParticles(double * particles, const int length, const int seedValue);
std::string convertToString(char* a,int size);
#endif
