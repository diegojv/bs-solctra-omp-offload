#include "solctra_multinode.h"
#include <cstdio>
#include <cmath>
#include <fstream>
#include <iostream>
#include <cstring>
#include <string>
#include <sstream>

#ifdef _OPENMP
	#include <omp.h>
#else
	#define omp_get_num_threads() 1
#endif

#pragma omp declare target
//old double* initializeGlobals(double*& rmi_f){
/*double* initializeGlobals(){
    int size_to_allocate = TOTAL_OF_COILS*LOOP_OF_GRADES*DIMENSIONS;
    //old rmi_f = static_cast<double*>(_mmm_malloc(sizeof(double) * (size_to_allocate), ALIGNMENT_SIZE));
    //rmi_f = static_cast<double*>(aligned_alloc(ALIGNMENT_SIZE, sizeof(double) * (size_to_allocate)));
    double* rmi_f = static_cast<double*>(malloc(sizeof(double)*size_to_allocate));
    return rmi_f;
}

void finishGlobal(double* rmi_f)
{
    //old _mmm_free(rmi_f);
    free(rmi_f);
}*/

double norm_of(double x,double y,double z){
    return sqrt(( x * x ) + ( y * y ) + ( z * z ));
}

#pragma omp end declare target

void load_coil_data(double* coils, const std::string& path)
{
#ifdef TRACE_ROCTX
    roctxRangePush("load_coil_data");
#endif
    for (int num = 0; num < TOTAL_OF_COILS; num++)
    {
	    std::ostringstream convert;
	    convert << num;
	    std::string value = convert.str();
	    std::string tmp =  path + "/Bobina"+value+"m.txt";
        loadCartesianFile(&(coils[num * LOOP_OF_GRADES*DIMENSIONS]), LOOP_OF_GRADES, tmp);
    }
#ifdef TRACE_ROCTX
    roctxRangePop();
#endif

}

void e_roof(double* coils, double* e_r,double* leng_segment)
{
    double x;
    double y;
    double z;
    for (int j = 0; j < TOTAL_OF_COILS; j++)
    {
        int coil_base = j * LOOP_OF_GRADES *DIMENSIONS;
    	for (int i = 0; i < TOTAL_OF_GRADES; i++)
            {
                int grade_base = i * DIMENSIONS;
                x = ( coils[coil_base + grade_base + DIMENSIONS] ) - ( coils[coil_base+grade_base] );
                y = ( coils[coil_base + grade_base + DIMENSIONS +1] ) - ( coils[coil_base+grade_base+1] );
                z = ( coils[coil_base + grade_base + DIMENSIONS +2] ) - ( coils[coil_base+grade_base+2] );
                leng_segment[j*LOOP_OF_GRADES + i] = norm_of(x,y,z);
                const double leng_segment_inverted = 1.0 / leng_segment[j*LOOP_OF_GRADES + i];
                e_r[coil_base+grade_base] = x * leng_segment_inverted;
                e_r[coil_base+grade_base+1] = y * leng_segment_inverted;
                e_r[coil_base+grade_base+2] = z * leng_segment_inverted;
            }
    }
}

void printeroof(GlobalData& data, const int subsetIndex, const std::string& outputPath){                                                                              
        std::ostringstream indexString;                                         
        indexString << subsetIndex;                                                
        std::string valueIndex = indexString.str();                                
        
        FILE* handler;                                                             
        std::string file_name = outputPath +  "/eroof" + valueIndex + ".txt";   
        handler = fopen(file_name.c_str(), "a");                                   
        if(handler == NULL)                                                        
        {                                                                          
            printf("Unable to open file=[%s]. Nothing to do\n", file_name.c_str());
            exit(0);                                                               
        }                                                                       
                                                                                   
                                                                                    
        for (int j = 0; j < TOTAL_OF_COILS; j++){                                        
            const int base = j * TOTAL_OF_GRADES;                            
            for (int i = 0; i < TOTAL_OF_GRADES; i++)                                   
            {                                                                      
                fprintf(handler, "%.17g\t%.17g\t%.17g\n", data.e_roof.x[base+i], data.e_roof.y[base + i], data.e_roof.z[base + i]);    
            }                                                                      
        }                                                                            
                                                                                   
        fclose(handler);                                                           
                                                                                   
}      

cartesian magnetic_field(double*coils,double* e_r,double* leng_segment, double* point)
{
    double Bx = 0;
    double By = 0;
    double Bz = 0;
    double rmi[3]={0.0,0.0,0.0};
    double rmf[3]={0.0,0.0,0.0};
    
    for (unsigned coil = 0; coil < TOTAL_OF_COILS; coil++)
    {
        const int coil_base = coil * LOOP_OF_GRADES*DIMENSIONS;
        double* coil_chunk = &(coils[coil_base]);
        rmi[0]=point[0] - coil_chunk[0];
        rmi[1]=point[1] - coil_chunk[1];
        rmi[2]=point[2] - coil_chunk[2];
        for (unsigned grade = 0; grade < TOTAL_OF_GRADES; grade++){
            if(grade>0){
                rmi[0]=rmf[0];
                rmi[1]=rmf[1];
                rmi[2]=rmf[2];
            }
            int grade_base = grade*DIMENSIONS;
            double* coil_subchunk = &(coil_chunk[grade_base]);
            rmf[0]=point[0] - coil_subchunk[DIMENSIONS];
            rmf[1]=point[1] - coil_subchunk[DIMENSIONS+1];
            rmf[2]=point[2] - coil_subchunk[DIMENSIONS+2];
            //TODO: Consider creation of a structure to avoid recalculation i element in rmi_f
            const double norm_Rmi = sqrt((( rmi[0] * rmi[0] ) + ( rmi[1] * rmi[1] ) + ( rmi[2] * rmi[2])));
            const double norm_Rmf = sqrt((( rmf[0] * rmf[0] ) + ( rmf[1] * rmf[1] ) + ( rmf[2] * rmf[2])));
            //firts vector of cross product in equation 8
            double u_x = MULTIPLIER * e_r[coil_base + grade_base];
            double u_y = MULTIPLIER * e_r[coil_base + grade_base+1];
            double u_z = MULTIPLIER * e_r[coil_base + grade_base+2];
            int segment_idx=coil*LOOP_OF_GRADES+grade;
            const double C =    ((( 2 * ( leng_segment[segment_idx] ) * ( norm_Rmi + norm_Rmf )) /
                                 ( norm_Rmi * norm_Rmf )) * (( 1 ) / (( norm_Rmi + norm_Rmf ) * ( norm_Rmi + norm_Rmf ) -
                                 leng_segment[segment_idx] * leng_segment[segment_idx] )));
            
            double v_x = rmi[0] * C;
            double v_y = rmi[1] * C;
            double v_z = rmi[2] * C;
            //cross product in equation 8
            Bx = Bx + (( u_y * v_z ) - ( u_z *v_y ));
            By = By - (( u_x * v_z ) - ( u_z *v_x ));
            Bz = Bz + (( u_x * v_y ) - ( u_y *v_x ));
        }
    }
    cartesian B = {0.0, 0.0, 0.0};
    B.x = Bx;
    B.y = By;
    B.z = Bz;

    return B;
}



void printIterationFile(double* particle_array, const int iteration, const std::string& output, const int particle_count){

    std::ostringstream convert;
    convert << iteration;
    std::string value = convert.str();

    FILE* handler;
    std::string file_name = output +"/"+ "iteration" + value + ".bin";
    handler = fopen(file_name.c_str(), "ab");
    if(nullptr == handler)
    {
        printf("Unable to open file=[%s]. Nothing to do\n", file_name.c_str());
        exit(0);
    }

    fwrite(particle_array, sizeof(double), particle_count*DIMENSIONS,handler);

    fclose(handler);
}

void printIterationFileTxt(double* particle_array, const int iteration, const std::string& output,const int length){
    std::ostringstream convert;
    convert << iteration;
    std::string value = convert.str();
    FILE* handler;
    std::string file_name = output +  "/iteration"+ value + ".txt";
    handler = fopen(file_name.c_str(), "a");
    if(nullptr == handler)
    {
        printf("Unable to open file=[%s]. Nothing to do\n", file_name.c_str());
        exit(0);
    }
    fprintf(handler, "x,y,z\n");
    for (int i=0; i<length; i++){
        int base = i*DIMENSIONS;
        fprintf(handler, "%e,%e,%e\n", particle_array[base], particle_array[base+1], particle_array[base+2]);
        
    }
    fclose(handler);
}

/*void printParallelIterationFile(double* particle_array, const int iteration, const std::string& output, const int rank_id, const int length, const int offset){

	std::ostringstream convert;
    convert << iteration;
    std::string value = convert.str();

    MPI_File handler;
	MPI_Status status;
	std::string file_name = output +  "/iteration" + value + ".bin";
    MPI_File_open(MPI_COMM_WORLD, file_name.c_str(), MPI_MODE_CREATE|MPI_MODE_WRONLY,MPI_INFO_NULL,&handler);
    if(nullptr == handler)
    {
        printf("Unable to open file=[%s]. Nothing to do\n", file_name.c_str());
        exit(0);
    }

	MPI_File_write_at(handler, offset*sizeof(double), particle_array, length, MPI_DOUBLE, &status);
	MPI_File_close(&handler);
}*/


/**
 * Compute next particle position
 *
 * This function implements the RK4 algorithm to approximate next particle
 * position. Divergence is checked in this function.
 *
 * @param data: contains the different coil data
 * @param start_point: reference to the particle for which we compute the next step
 * @param step_size: the size of each simulation step (measured in meters)
 * @param mode: mode refers to whether divergence is checked or not. If not, all steps are performed for all particles
 * @return void function. Particle position is updated in place using the particle reference
 */
bool computeIteration(double* coils,double* e_r,double* leng_segment, double* start_point, const double& step_size, const int mode, int &divergenceCounter)
{
    bool diverged=false;
    double p1[DIMENSIONS];
    double p2[DIMENSIONS];
    double p3[DIMENSIONS];
    cartesian K1;
    cartesian K2;
    cartesian K3;
    cartesian K4;

    cartesian Ovect = {0, 0, 0};
    Particle p = {0, 0, 0};
    cartesian r_vector;
    double norm_temp;
    double r_radius;

    const double half = 1.0 / 2.0;

    K1 = magnetic_field(coils,e_r,leng_segment, start_point);
    norm_temp = 1.0 / norm_of(K1.x,K1.y,K1.z);
    K1.x = ( K1.x * norm_temp ) * step_size;
    K1.y = ( K1.y * norm_temp ) * step_size;
    K1.z = ( K1.z * norm_temp ) * step_size;
    p1[0] = ( K1.x * half ) + start_point[0];
    p1[1] = ( K1.y * half ) + start_point[1];
    p1[2] = ( K1.z * half ) + start_point[2];
    

    K2 = magnetic_field(coils,e_r,leng_segment, p1);
    norm_temp = 1.0 / norm_of(K2.x,K2.y,K2.z);
    K2.x = ( K2.x * norm_temp ) * step_size;
    K2.y = ( K2.y * norm_temp ) * step_size;
    K2.z = ( K2.z * norm_temp ) * step_size;
    p2[0] = ( K2.x * half ) + start_point[0];
    p2[1] = ( K2.y * half ) + start_point[1];
    p2[2] = ( K2.z * half ) + start_point[2];
    
    K3 = magnetic_field(coils,e_r,leng_segment, p2);
    norm_temp = 1.0 / norm_of(K3.x,K3.y,K3.z);
    K3.x = ( K3.x * norm_temp ) * step_size;
    K3.y = ( K3.y * norm_temp ) * step_size;
    K3.z = ( K3.z * norm_temp ) * step_size;
    p3[0] = K3.x + start_point[0];
    p3[1] = K3.y + start_point[1];
    p3[2] = K3.z + start_point[2];

    K4 = magnetic_field(coils,e_r,leng_segment, p3);
    norm_temp = 1.0 / norm_of(K4.x,K4.y,K4.z);
    K4.x = ( K4.x * norm_temp ) * step_size;
    K4.y = ( K4.y * norm_temp ) * step_size;
    K4.z = ( K4.z * norm_temp ) * step_size;
    start_point[0] = start_point[0] + (( K1.x + 2 * K2.x + 2 * K3.x + K4.x ) / 6 );
    start_point[1] = start_point[1] + (( K1.y + 2 * K2.y + 2 * K3.y + K4.y ) / 6 );
    start_point[2] = start_point[2] + (( K1.z + 2 * K2.z + 2 * K3.z + K4.z ) / 6 );
    
    


    if (mode == 1)
    {
        p.x = start_point[0];
        p.y = start_point[1];
        Ovect.x = ( p.x / norm_of(p.x,p.y,p.z)) * 0.2381; //// Origen vector
        Ovect.y = ( p.y / norm_of(p.x,p.y,p.z)) * 0.2381;
        Ovect.z = 0;
        r_vector.x = start_point[0] - Ovect.x;
        r_vector.y = start_point[1] - Ovect.y;
        r_vector.z = start_point[2] - Ovect.z;
        
        r_radius = norm_of(r_vector.x,r_vector.y,r_vector.z);
        if (r_radius > 0.0944165)
        {
            start_point[0] = MINOR_RADIUS;
	        start_point[1] = MINOR_RADIUS;
	        start_point[2] = MINOR_RADIUS;
            divergenceCounter += 1;
            diverged=true;
        }
     }
    return diverged;
}

/*
void getMagneticProfile(const GlobalData& data, const int num_points, const int phi_angle, const std::string& output, const int dimension){

 	//Prepare parameters for magnetic_field function: rmi, rmf
	Coil rmi_f[TOTAL_OF_COILS];
    Particle* observation_points;
	Particle point={0,0,0};
	cartesian B_point;
	const double major_R = 0.2381;
	const double minor_r = 0.0944165;
	double width;
 	double radians = phi_angle*PI/180.0;

    initializeGlobals(rmi_f);

    //Prepare output file
    FILE* handler;
    std::string file_name = output + "/magnetic_field.txt";
    std::cout << "Before Handler open" << std::endl;
    handler = fopen(file_name.c_str(), "w");
    std::cout << "After handler open" << std::endl;
    if(nullptr == handler)
    {
        printf("Unable to open file=[%s]. Nothing to do\n", file_name.c_str());
        exit(0);
    }

    fprintf(handler, "x,y,z,|B|\n");

    if(dimension == 1){
        width = (2*minor_r)/num_points;
        observation_points = static_cast<Particle*>(malloc(sizeof(struct Particle) * num_points));
        //Generate observation points at phi_angle plane
        for(int i=0; i<num_points; i++){
            observation_points[i].x = ((major_R-minor_r+(width*i))+minor_r*cos(PI/2))*cos(radians);
            observation_points[i].y = ((major_R-minor_r+(width*i))+minor_r*cos(PI/2))*sin(radians);
            observation_points[i].z = 0.0;
        }
        for(int i=0;i<num_points;i++)
        {
            point.x = observation_points[i].x;
            point.y = observation_points[i].y;
            point.z = observation_points[i].z;
            B_point = magnetic_field(rmi_f,data,point);
            fprintf(handler, "%e,%e,%e,%e\n", point.x, point.y, point.z,norm_of(B_point));
        }
    }else if(dimension == 2){
        std::cout << "Entrando a dimension 2" << std::endl;
		width = minor_r/num_points;
        observation_points = static_cast<Particle*>(malloc(sizeof(struct Particle) * (num_points*360)));
        std::cout << "Inicializando puntos de observacion" << std::endl;
		for(int i=0; i<360; i++){
            for(int j=0; j<num_points; j++){
            //std::cout << "it:  " << ((num_points*i)+j) << std::endl;
	    	observation_points[((num_points*i)+j)].x = (major_R+((width*j)*sin(i*(PI/180))))*cos(radians);
            observation_points[((num_points*i)+j)].y = ((width*j)*cos(i*PI/180));
            observation_points[((num_points*i)+j)].z= (major_R+(width*j)* sin(i*PI/180))*sin(radians);
            }
        }
	std::cout << "Inicializacion finalizada" << std::endl;

        for(int i=0;i<num_points*360;i++)
        {
            point.x = observation_points[i].x;
            point.y = observation_points[i].y;
            point.z = observation_points[i].z;
            B_point = magnetic_field(rmi_f,data,point);
            fprintf(handler, "%e,%e,%e,%e\n", point.x, point.y, point.z,norm_of(B_point));
        }
	std::cout << "Campo calculado" << std::endl;

    }
     //For each observation point call magnetic_field
	fclose(handler);
	free(observation_points);
	finishGlobal(rmi_f);
}*/

/**
 * Run the actual simulation steps
 *
 * This function coordinates execution of simulation steps.
 * 
 * @param data: contains the different coil data
 * @param output: string containing the output directory path ("results_<jobId>")
 * @param particles: Coil structure containing x,y and z arrays of particle positions
 * @param particle_count: particle_count of particle arrays (number of particles to simulate)
 * @param steps: amount of simulation steps to perform
 * @param step_size: the size of each simulation step (measured in meters)
 * @param mode: mode refers to whether divergence is checked or not. If not, all steps are performed for all particles
 * @param print_type: decides if printing is done with tabs or using commas as separation tokens
 * @param startPosition: used to assign names to output files when printing them by particle
 * @return void function. However, an output file per iteration is generated.
 */
void runParticles(double* coils,double* e_r,double* leng_segment, const std::string& output, double* particles, const int particle_count, const int steps, const double step_size,
const int mode,const int debugFlag)
{
#ifdef TRACE_ROCTX
    roctxRangePush("runParticles");
#endif
	//int number_of_threads = omp_get_max_threads();
    int divergenceCounter=0;
    bool diverged = false;
	int particleIndex;
    double startIOTime = 0;
    double endIOTime = 0;
	double totalIOTime = 0;
    double compStartTime = 0;
    double compHalfTime = 0;
    double compEndTime = 0;
    double rankCompTime = 0;
    double balancedTotalCompTime = 0;
    double secondCompTime = 0; 
    double balancedTime = 0;
    double totalCompTime = 0;

    int size_3D = DIMENSIONS*LOOP_OF_GRADES*TOTAL_OF_COILS;
    int size_2D = LOOP_OF_GRADES*TOTAL_OF_COILS;
    int size_particles = particle_count*DIMENSIONS;
    
    
    //printIterationFile(particles, 0, output,particle_count);
#ifdef TRACE_ROCTX
    roctxRangePush("memcpyH2D");
#endif
#pragma omp target enter data map(to:coils[0:size_3D],e_r[0:size_3D],leng_segment[0:size_2D],particles[0:size_particles])
#ifdef TRACE_RCOTX
    roctxRangePop();
#endif
    //#pragma omp parallel num_threads(2)
    //{
      //  #pragma omp single
        //{
            for (int i = 1; i <= steps; i++){
#ifdef TRACE_ROCTX
                roctxRangePush("runParticles_kernel");	        
#endif
                #pragma omp target teams distribute parallel for
                for(int p=0; p < particle_count ; p++){
                    int base = p*DIMENSIONS;
                    if((particles[base] == MINOR_RADIUS) && (particles[base+1] == MINOR_RADIUS) && (particles[base+2] == MINOR_RADIUS)){
                        continue;
                    }
                    else{
                        diverged = computeIteration(coils,e_r,leng_segment,&particles[base],step_size,mode,divergenceCounter);
                    }
                }
#ifdef TRACE_ROCTX
                roctxRangePop();
#endif 
                /*if(i%10 == 0){
                    #pragma omp taskwait
                    #pragma omp target update from(particles[0:size_particles])
                    {
                        #pragma omp task shared(particles) firstprivate(i,output,size_particles)
    			printIterationFile(particles, i, output,particle_count);
                    }
                }*/
            }
        //}
    //}
#ifdef TRACE_ROCTX
    roctxRangePush("memcpyD2H");
#endif
    #pragma omp target exit data map(release:coils[0:size_3D],e_r[0:size_3D],leng_segment[0:size_2D],particles[0:size_particles])
#ifdef TRACE_ROCTX
    roctxRangePop();
    roctxRangePop();
#endif
}
