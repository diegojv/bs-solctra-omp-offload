# GPU OpenMP offload prescriptive implementation

- To compile use the provided Makefile. Select the appropiate compiler depending on your platform
- Execution examples are provided in: singleGPUSmall.slurm, singleGPUMedium.slurm singleGPULarge.slurm
    - An example command line:
    ./bs-gpu -length 512000 -id 777 -resource resources -mode 1 -d 0 -steps 100
