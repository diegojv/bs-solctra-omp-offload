#include <fstream>
#include <iostream>
#include <cstring>
#include <string>
#include <sstream>
#include <math.h>
#include <ctime>
#include "timer.h"
#include "solctra_multinode.h"
#include "omp.h"
#ifdef TRACE_ROCTX
#include "roctracer_ext.h"
#include "roctx.h"
#endif
const unsigned DEFAULT_STRING_BUFFER = 100;
const unsigned DEFAULT_STEPS = 200; //100000
const double DEFAULT_STEP_SIZE = 0.001;
const unsigned DEFAULT_PRECISION = 5;
const unsigned DEFAULT_PARTICLES= 1;
const unsigned DEFAULT_MODE= 1;
const std::string DEFAULT_OUTPUT = "results";
const std::string DEFAULT_RESOURCES = "resources";
const unsigned DEFAULT_MAGPROF = 0;
const unsigned DEFAULT_NUM_POINTS = 10000;
const unsigned DEFAULT_PHI_ANGLE = 0;
const unsigned DEFAULT_DIMENSION = 1;
const unsigned DEFAULT_DEBUG = 0;



unsigned getPrintPrecisionFromArgs(const int& argc, char** argv)
{
    for(int i = 1 ; i < argc - 1 ; ++i)
    {
        std::string tmp = argv[i];
        if(tmp == "-precision")
        {
            return static_cast<unsigned>(atoi(argv[i+1]));
        }
    }
    return DEFAULT_PRECISION;
}
unsigned getStepsFromArgs(const int& argc, char** argv)
{
    for(int i = 1 ; i < argc - 1 ; ++i)
    {
        std::string tmp = argv[i];
        if(tmp == "-steps")
        {
            return static_cast<unsigned>(atoi(argv[i+1]));
        }
    }
    return DEFAULT_STEPS;
}
double getStepSizeFromArgs(const int& argc, char** argv)
{
    for(int i = 1 ; i < argc - 1 ; ++i)
    {
        std::string tmp = argv[i];
        if(tmp == "-stepSize")
        {
            return strtod(argv[i+1], nullptr);
        }
    }
    return DEFAULT_STEP_SIZE;
}

void LoadParticles(const int& argc, char** argv, double* particles, const int length, const int seedValue)
{
    #ifdef TRACE_ROCTX
    roctxRangePush("LoadParticles");
    #endif
    bool found = false;
    for(int i = 1 ; i < argc - 1 ; ++i)
    {
        std::string tmp = argv[i];
        if(tmp == "-particles")
        {
            loadParticleFile(particles, length, argv[i+1]);
            found = true;
            break;
        }
    }
    if(!found)
    {
        printf("No file given. Initializing random particles\n");
        initializeParticles(particles, length,seedValue);
    }
#ifdef TRACE_ROCTX
    roctxRangePop();
#endif 
}

std::string getResourcePath(const int& argc, char** argv)
{
    for(int i = 1 ; i < argc - 1 ; ++i)
    {
        std::string param = argv[i];
        if("-resource" == param)
        {
            return std::string(argv[i+1]);
        }
    }
    return DEFAULT_RESOURCES;
}

unsigned getParticlesLengthFromArgs(const int& argc, char** argv)
{
    for(int i = 1 ; i < argc - 1 ; ++i)
    {
        std::string tmp = argv[i];
        if(tmp == "-length")
        {
            return static_cast<unsigned>(atoi(argv[i+1]));
        }
    }
    printf("ERROR: You must specify number of particles to simulate\n");
    exit(1);
}

unsigned getDebugFromArgs(const int& argc, char** argv)
{
    for(int i = 1 ; i < argc - 1 ; ++i)
    {
        std::string tmp = argv[i];
        if(tmp == "-d")
        {
            return static_cast<unsigned>(atoi(argv[i+1]));
        }
    }
    return DEFAULT_DEBUG;
}

unsigned getModeFromArgs(const int& argc, char** argv)
{
    for(int i = 1 ; i < argc - 1 ; ++i)
    {
        std::string tmp = argv[i];
        if(tmp == "-mode")
        {
            return static_cast<unsigned>(atoi(argv[i+1]));
        }
    }
    return DEFAULT_MODE;
}

std::string getJobId(const int& argc, char** argv)
{
    for(int i = 1 ; i < argc - 1 ; ++i)
    {
        std::string tmp = argv[i];
        if(tmp == "-id")
        {
            return std::string(argv[i+1]);
        }
    }
    printf("ERROR: job id must be given!!\n");
    exit(1);
}

unsigned getMagneticProfileFromArgs(const int& argc, char** argv)
{
    for(int i = 1 ; i < argc - 1 ; ++i)
    {
     	std::string tmp = argv[i];
        if(tmp == "-magnetic_prof")
        {
            return static_cast<unsigned>(atoi(argv[i+1]));
        }
    }
    return DEFAULT_MAGPROF;
}


unsigned getNumPointsFromArgs(const int& argc, char** argv)
{
    for(int i = 1 ; i < argc - 1 ; ++i)
    {
     	std::string tmp = argv[i];
        if(tmp == "-magnetic_prof")
        {
            return static_cast<unsigned>(atoi(argv[i+2]));
        }
    }
    return DEFAULT_NUM_POINTS;
}

unsigned getAngleFromArgs(const int& argc, char** argv)
{
    for(int i = 1 ; i < argc - 1 ; ++i)
    {
       	std::string tmp = argv[i];
        if(tmp == "-magnetic_prof")
        {
     	    return static_cast<unsigned>(atoi(argv[i+3]));
        }
    }
    return DEFAULT_PHI_ANGLE;
}

unsigned getDimension(const int& argc, char** argv)
{
    for(int i = 1 ; i < argc - 1 ; ++i)
    {
     	std::string tmp = argv[i];
        if(tmp == "-magnetic_prof")
        {
            return static_cast<unsigned>(atoi(argv[i+4]));
        }
    }
    return DEFAULT_DIMENSION;
}


int main(int argc, char** argv)
{
#ifdef TRACE_ROCTX
    roctxRangePush("main");
#endif 
    /*******Declaring program and runtime parameters*************/
    std::string resourcePath; //Coil directory path
    unsigned steps; //Amount of simulation steps
    double stepSize; //Size of each simulation step

    /*Variables for magnetic profile diagnostic*/
    unsigned magprof; //Flag to control whether magnetic profile is computed
    unsigned num_points; //Number of sampling points for magnetic profile
    unsigned phi_angle; //Angle at which the magnetic profile will be computed
    /******************************************/

    unsigned precision; //TBD
    unsigned int length; //Amount of particles to simulate
    unsigned int debugFlag;
    unsigned int mode; //Check divergence of simulation or not
    unsigned int dimension;
    std::string output; //Path of results directory
    std::string jobId; //JobID in the cluster
    std::ofstream handler;
    /*******Declaring program and runtime parameters*************/
#ifdef TRACE_ROCTX 
    roctxRangePush("getRuntimeParameters");
#endif
    resourcePath = getResourcePath(argc, argv);
    steps = getStepsFromArgs(argc, argv);
    stepSize = getStepSizeFromArgs(argc, argv);
    precision = getPrintPrecisionFromArgs(argc, argv);
    length = getParticlesLengthFromArgs(argc, argv);
    mode = getModeFromArgs(argc, argv);
    debugFlag = getDebugFromArgs(argc, argv);
    magprof = getMagneticProfileFromArgs(argc, argv);
    num_points = getNumPointsFromArgs(argc, argv);
    phi_angle = getAngleFromArgs(argc, argv);
    jobId = getJobId(argc, argv);
    dimension = getDimension(argc,argv);
#ifdef TRACE_ROCTX
    roctxRangePop();
#endif

#ifdef TRACE_ROCTX
    roctxRangePush("describeExecution");
#endif

    output = "results_" + jobId;
    createDirectoryIfNotExists(output);
    std::cout.precision(precision);
    std::cout << "Running with:" << std::endl;
    std::cout << "Resource Path=[" << resourcePath << "]." << std::endl;
    std::cout << "JobId=[" << jobId << "]." << std::endl;
    std::cout << "Steps=[" << steps << "]." << std::endl;
    std::cout << "Steps size=[" << stepSize << "]." << std::endl;
    std::cout << "Particles=[" << length << "]." << std::endl;
    std::cout << "Input Current=[" << I << "] A." << std::endl;
    std::cout << "Mode=[" << mode << "]." << std::endl;
    std::cout << "Output path=[" << output << "]." << std::endl;
    std::string file_name = "stdout_"+jobId+".log";

    handler.open(file_name.c_str());
    if(!handler.is_open()){
        std::cerr << "Unable to open stdout.log for appending. Nothing to do." << std::endl;
        exit(0);
    }

    handler << "Running with:" << std::endl;
    handler << "Steps=[" << steps << "]." << std::endl;
    handler << "Steps size=[" << stepSize << "]." << std::endl;
    handler << "Particles=[" << length << "]." << std::endl;
    handler << "Mode=[" << mode << "]." << std::endl;
    handler << "Output path=[" << output << "]." << std::endl;
#ifdef TRACE_ROCTX
    roctxRangePop();
#endif

    double startInitializationTime = 0;
    double endInitializationTime = 0;
    
    double* particles;
    particles = static_cast<double*>(_mmm_malloc(sizeof(double)*length*DIMENSIONS, ALIGNMENT_SIZE));
    if(debugFlag){
    	StartTimer();
    }
    
    LoadParticles(argc, argv, particles, length,0);
    
    if(debugFlag){
	endInitializationTime = GetTimer()/1000.0;
        std::cout << "Total initialization time=[" << (endInitializationTime - startInitializationTime) << "]." << std::endl;
    }
    printf("Particles initialized\n");
    
    const size_t sizeToAllocate = sizeof(double) * LOOP_OF_GRADES * TOTAL_OF_COILS*DIMENSIONS;
    double* coils = static_cast<double*>(_mmm_malloc(sizeToAllocate, ALIGNMENT_SIZE));
    double* e_r = static_cast<double*>(_mmm_malloc(sizeToAllocate, ALIGNMENT_SIZE));
    double* leng_segment = static_cast<double*>(_mmm_malloc(TOTAL_OF_COILS*LOOP_OF_GRADES*sizeof(double), ALIGNMENT_SIZE));
    
    load_coil_data(coils, resourcePath);
    std::cout << "Coil data loaded" << std::endl;
    e_roof(coils, e_r,leng_segment);
    
    double startTime = 0;
    double endTime = 0;
    
    startTime= omp_get_wtime();
    std::cout << "Executing simulation" << std::endl;
    
    runParticles(coils,e_r,leng_segment, output, particles, length, steps, stepSize, mode, debugFlag);
    
    endTime= omp_get_wtime();
/******************End of the trajectory simulation********************/
    _mmm_free(coils);
    _mmm_free(e_r);
    _mmm_free(leng_segment);
    _mmm_free(particles);
   std::cout << "Simulation finished" << std::endl;
   std::cout << "Total execution time=[" << (endTime - startTime) << "]." << std::endl;
   handler << "Total execution time=[" << (endTime - startTime) << "]." << std::endl;
   handler.close();
   /*handler.open("stats.csv", std::ofstream::out | std::ofstream::app);
   if(!handler.is_open())
   {
       std::cerr << "Unable to open stats.csv for appending. Nothing to do." << std::endl;
       exit(0);
   }
   handler << jobId << "," << length << "," << steps << "," <<  stepSize << "," << output << "," << (endTime - startTime) << std::endl;
   handler.close();*/
   time_t now = time(0);
   char* dt = ctime(&now);
   std::cout << "Timestamp: " << dt << std::endl;
#ifdef TRACE_ROCTX 
   roctxRangePop();
#endif
    return 0;
}
